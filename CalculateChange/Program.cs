﻿using System;
using System.Collections.Generic;

namespace nu.educom
{
    partial class Program
    {
        /// <summary>
        /// Start of the program
        /// </summary>
        /// <param name="args"></param> (not used)
        static void Main(string[] args) {

            var initialContent = new SortedDictionary<decimal, int>(new ReverseComparer()) {
                {    5m,  1 }, //  1 bill  of  5 euro
                {    2m,  1 }, //  1 coin  of  2 euro
                {    1m,  4 }, //  4 coins of  1 euro
                {   50m,  0 }, // no bills of 50 euro
                {   20m,  1 }, //  1 bill  of 20 euro
                {   10m,  0 }, // no bills of 10 euro
                {  0.5m,  2 }, //  2 coins of 50 cent
                {  0.2m, 10 }, // 10 coins of 20 cent
                {  0.1m,  3 }, //  3 coins of 10 cent
                { 0.05m,  7 }, //  7 coins of  5 cent
                { 0.02m,  1 }, //  1 coin  of  2 cent
                { 0.01m,  4 }, //  4 coins of  1 cent
            };

            var register = new CashRegister(initialContent);

            var result = register.MakeChange(4.51m, 10);
            ShowAmount("MakeChange for 4,51 from 10 euro", result); // 5, .20, .20, .05, .02, .01, .01

            result = register.MakeChange(1.30m, 5);
            ShowAmount("MakeChange for 1,30 from 5 euro", result); // 2, 1, .50, .20

            result = register.MakeChange(0.97m, 20);
            ShowAmount("MakeChange for 0,97 from 20 euro", result); // 10, 5, 1, 1, 1, .50, .20, .20, .10

            var profit = register.GetProcessEndOfDay();

            ShowProfit(profit);

            if (!profit.Equals(6.76m)) {
                throw new InvalidProgramException("Expected different amount");
            }

            result = register.MakeChange(1.30m, 20.50m);
            ShowAmount("MakeChange of 1,30 from 20 euro", result); // No change possible

            result = register.MakeChange(1.30m, 2.50m);
            ShowAmount("MakeChange of 1,30 from 2,50 euro", result); // ..20, .20, .20, .20, .20, .20, .10, .10

            profit = register.GetProcessEndOfDay();

            ShowProfit(profit);

            if (!profit.Equals(1.30m)) {
                throw new InvalidProgramException("Expected different amount on second day");
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Show the amount to the console, when a <code>null</code> is passed, it shows "failed to make change"
        /// </summary>
        /// <param name="msg">The message to display prior to the amounts</param>
        /// <param name="change">The change to display or <code>null</code> when failed to make change</param>
        private static void ShowAmount(string msg, IDictionary<decimal, int> change) {
            Console.Write(msg);
            if (change==null)
            {
                Console.WriteLine("Failed to make change");
            }else{
                foreach(decimal key in change.Keys)
                {
                    Console.Write(" ");
                    Console.Write(change[key]);
                    Console.Write(" x ");
                    Console.Write(key);
                    Console.Write(" euro");
                }
                Console.WriteLine(".");
            }
        }
        /// <summary>
        /// Show the profit of the day
        /// </summary>
        /// <param name="profit">The profit to show</param>
        private static void ShowProfit(decimal profit) {
            Console.WriteLine(profit);
        }
    }
}
