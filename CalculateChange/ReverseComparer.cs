﻿using System.Collections.Generic;

namespace nu.educom
{
    /// <summary>
    /// Helper class to make the sorted dicionary order the keys in descending order (highest denomination first)
    /// </summary>
    public class ReverseComparer : IComparer<decimal>
    {
        public int Compare(decimal x, decimal y) {
            return y.CompareTo(x);
        }
    }
}
