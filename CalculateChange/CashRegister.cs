﻿using System;
using System.Collections.Generic;

namespace nu.educom
{
    class CashRegister
    {
        private IDictionary<decimal, int> till;
        private decimal startAmount;

        /// <summary>
        /// Create a cachRegister drawer that keeps track of all notes and coins in it's drawer 
        /// </summary>
        /// <param name="initialChange">The initial content of the the cash regisiter: 'denomination' => amount of bills/coins, 
        ///                             ordered descending on nomination.</param>
        public CashRegister(IDictionary<decimal, int> initialChange)
         {
            till = new SortedDictionary<decimal, int>(initialChange, new ReverseComparer());
            startAmount = CountTill();
             
        }

        /// <summary>
        /// Calculate the change of the the amount to pay and return a list of returned notes and coins and their amount
        /// </summary>
        /// <example>
        /// MakeChange(4.51m, 10) => { { 5.0, 1 }, { 0.2, 2 }, { 0.05, 1 }, { 0.01, 4 } } 
        /// meaning: one "note of 5 euro", two "coins of 20 cent", one "coin of 5 cent" and four "coins of 0.01 cent".
        /// </example>
        /// <remarks>When change is made, the cash register counts are updated</remarks>
        /// <remarks>When there are no 0.01 or 0.02 cents coins left to make change, the amount is rounded to 0.05 cents</remarks>
        /// <param name="price">amount to pay</param>
        /// <param name="paid">the amount paid</param>
        /// <returns>a list of all notes and coins that make up the change or 
        ///          <code>NULL</code> if failed to make change from the cash register </returns>
        public IDictionary<decimal, int> MakeChange(decimal price, decimal paid)
        {
            decimal amountToReturn = paid - price;
            decimal moneyReceived = paid;
            IDictionary<decimal, int> received = new SortedDictionary<decimal, int>(new ReverseComparer());
            IDictionary<decimal, int> change = new SortedDictionary<decimal, int>(new ReverseComparer());

            foreach (decimal key in till.Keys)
            {
                int nrOfCoins = till[key];
                while (amountToReturn >= key && nrOfCoins > 0)
                {
                    nrOfCoins -= 1;
                    amountToReturn -= key;
                    if (change.ContainsKey(key))
                    {
                        change[key] += 1;
                    }
                    else
                    {
                        change.Add(key, 1);
                    }
                }
                while (paid >= key)
                {
                    paid -= key;
                    if (received.ContainsKey(key))
                    {
                        received[key] += 1;
                    }
                    else
                    {
                        received.Add(key, 1);
                    }
                }
            }

            //round up functionality
            if (amountToReturn >0)
            {
                /* JH: Bij nader inzien gaat onderstaande code soms mis, omdat we alleen de centen en twee-centen moeten weghalen die ons over een veelvoud van 0,05 heen brengen.
                 *     De andere centen moeten we laten staan, maar hoe we dat moeten doen is mij even nog niet duidelijk. */
                change.Remove(0.01m);
                change.Remove(0.02m);

                //currentChange is the amount of change we have from before rounding up/down the price
                decimal currentChange = CountDictionairy(change);
                //newPrice is the new price after we round up/down old price
                decimal newPrice = Decimal.Round(price*20)/20;
                //newChange s the new amount of change to return after the round up/down
                decimal newChange = moneyReceived - newPrice;
                //remainingChange is the change we have to return after 
                //substracting the old change from the new change
                decimal remainingAmount = newChange - currentChange;

                if (remainingAmount > 0.05m)
                {
                    return null;
                } else if (remainingAmount == 0.05m) {
                    
                    //if the change dictionary contains a key for 0.05
                    if (change.ContainsKey(0.05m)) {
                        //if there is a key in change and the result of 
                        //substracting the value of till from change
                        //results in 0 or lower
                        if ((till[0.05m] - change[0.05m]) < 1) { 
                            //return null, meaning we can't make change
                            return null;
                        } else {      
                            //if not equal to 0 or lower add 1 value to key in change
                            change[0.05m] += 1;
                        }
                        //if there is no key in change for 0.005
                        //and the till contains 0 or lower as a value for 0.05
                    } else if (till[0.05m] < 1) {
                        //return null, meaning we can't make change
                        return null;
                    } else { 
                        // add key 0.05 to change dictionary and set value to 1
                        change.Add(0.05m, 1);
                    }
                }
                //if the remaining amount is not 0
                else if (remainingAmount != 0)
                {
                    //return null, meaning we can't make change
                    return null;
                }
            }

            //update the till according to give change
            foreach (decimal key in change.Keys)
            {
                //substract the value of the key of change 
                //from the value of the key in till
                till[key] -= change[key];
            }
            foreach (decimal key in received.Keys)
            {
                //add the value of the key of received
                //to the value of the key in till
                till[key] += received[key];
            }
            return change;
        }

        /// <summary>
        /// Calculate the total amount of cash added to the register since creation until
        /// last call to processEndOfDay by substracting the end amount from the start amount.
        /// <remark>once called it will update startAmount to endAmount for the next day</remark>
        /// </summary>
        /// <returns>the total amount added</returns>
        public decimal GetProcessEndOfDay()
        {
            decimal    endAmount = CountTill(); 
            decimal newAmount = endAmount - startAmount;
            startAmount = endAmount; 
            return newAmount;
        }

        /// <summary>
        /// Count the current till and return the sum value
        /// </summary>
        /// <returns>the sum of everything in the till</returns>
        private decimal CountTill()
        {
            return CountDictionairy(till);
        }

        /// <summary>
        /// reads dictionaries and calculates a total amount based on key*value
        /// </summary>
        /// <param name="dictionary">the IDictionary to read from</param>
        /// <returns>
        /// returns the total amount calculated
        /// </returns>
        private decimal CountDictionairy(IDictionary<decimal, int> dictionary)
        {
            decimal total = 0m;

            foreach (KeyValuePair<decimal,int> element in dictionary)    
            {
                total +=  (element.Key * element.Value);
            }
            return total;
        }
    }
}
