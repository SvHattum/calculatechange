﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace nu.educom
{
    [TestFixture]
    public class CashRegisterTest
    { 

        [Test]
        public void TestConstructorSucceeded()
        {
            //prepare
            var initialContentTest = new SortedDictionary<decimal, int>(new ReverseComparer()) {
                {1m,2 }
            };
            
            //run
            CashRegister cr = new CashRegister(initialContentTest);
            //validate
            Assert.IsNotNull(cr);
        }

        [Test]
        public void TestConstructorFailed()
        {
            //prepare
            //run
            ///<remarks>Assert.Throws<ArgumentNullException>(() => new CashRegister(null));
            ///above the test is a newer way to test exceptions</remarks>
            try
            {
                CashRegister cr = new CashRegister(null);
                Assert.Fail();
            }
            catch (Exception e)
            {
                //validate
                Assert.IsInstanceOf<ArgumentNullException>(e);
            }
        }
        [Test]
        public void TestMakeChangeSucceeded()
        {
            ///<remarks>the initial till always needs to have keys defined for all currency, else it will
            ///fail to add the specific coin it received as payment to the specific key in till and will 
            ///subdivide it into a a lower key. should this be changed?</remarks>
            //prepare
            var initialContentTest = new SortedDictionary<decimal, int>(new ReverseComparer()) {
                {5m,0 },
                {1m,2 },
                {0.05m,1 }
            };
            var expectedResult = new SortedDictionary<decimal, int>(new ReverseComparer())
            {
                {1m,1 },
                {0.05m, 1 }
            };
            //run
            CashRegister cr = new CashRegister(initialContentTest);
            IDictionary<decimal, int> result = cr.MakeChange(3.95m, 5);
            //validate 
            Assert.AreEqual(expectedResult, result);
        }
        /* JH: Mis nog een test waarbij een tweede MakeChange wordt aangeroepen die geld nodig heeft wat bij de eerste MakeChange is betaald */

        [Test]
        public void TestMakeChangeFailed()
        {
            //prepare
            var initialContentTest = new SortedDictionary<decimal, int>(new ReverseComparer()) {
                {5m,0 },
                {1m,1 },
                {0.05m,0 }
            };
            /* JH: Deze heb je niet nodig */
            var expectedResult = new SortedDictionary<decimal, int>(new ReverseComparer())
            {
                {1m,1 },
                {0.05m, 1 }
            };
            //run
                CashRegister cr = new CashRegister(initialContentTest);
                IDictionary<decimal, int> result = cr.MakeChange(3.95m, 5);
            //validate
            Assert.IsNull(result);
        }
        /* JH: Mis nog een test waarbij MakeChange wordt aangeroepen met 2x hetzelfde bedrag */
        /* JH: Mis nog een test waarbij MakeChange wordt aangeroepen waarbij het eerste bedrag groter is dan het tweede bedrag */

        [Test]
        public void TestGetProcessEndOfDay()
        {
            //prepare
            var initialContentTest = new SortedDictionary<decimal, int>(new ReverseComparer()) {
                {5m,1 },
                {1m,1 },
                {0.05m,2 }
            };
            /* JH: Deze heb je niet nodig */
            var expectedResult = new SortedDictionary<decimal, int>(new ReverseComparer())
            {
                {1m,1 },
                {0.05m, 1 }
            };
            var expectedProfit = 3.95m;
            //run
            CashRegister cr = new CashRegister(initialContentTest);
            IDictionary<decimal, int> result = cr.MakeChange(3.95m, 5);
            decimal profit = cr.GetProcessEndOfDay();
            //validate
            Assert.AreEqual(expectedProfit, profit);
        }

        /* JH: Mis nog een test waarbij GetProcessEndOfDay 2x wordt aangroepen steeds met een andere MakeChange ertussen */
        /* JH: Mis nog een test waarbij een makechange faalt en dan de GetProcessEndOfDay 0 retourneerd */

    }
}
